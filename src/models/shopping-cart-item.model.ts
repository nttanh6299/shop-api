import {Entity, model, property} from '@loopback/repository';

@model()
export class ShoppingCartItem extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  productId: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'number',
    required: true,
  })
  quantity: number;


  constructor(data?: Partial<ShoppingCartItem>) {
    super(data);
  }
}

export interface ShoppingCartItemRelations {
  // describe navigational properties here
}

export type ShoppingCartItemWithRelations = ShoppingCartItem & ShoppingCartItemRelations;
