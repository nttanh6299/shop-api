import {Entity, model, property} from '@loopback/repository';
import {ShoppingCartItem} from './shopping-cart-item.model';

@model()
export class ShoppingCart extends Entity {
  @property.array(ShoppingCartItem)
  items?: ShoppingCartItem[];

  @property({id: true})
  userId: string;

  constructor(data?: Partial<ShoppingCart>) {
    super(data);
  }
}

export interface ShoppingCartRelations {
  // describe navigational properties here
}

export type ShoppingCartWithRelations = ShoppingCart & ShoppingCartRelations;
