import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {ShoppingCart, ShoppingCartRelations} from '../models';

export class ShoppingCartRepository extends DefaultCrudRepository<
  ShoppingCart,
  typeof ShoppingCart.prototype.userId,
  ShoppingCartRelations
> {
  constructor(@inject('datasources.db') dataSource: DbDataSource) {
    super(ShoppingCart, dataSource);
  }

  async deleteItemsByUserId(userId: string) {
    const cart = await this.findOne({where: {userId}});

    if (cart && cart.items && cart.items.length > 0) {
      cart.items = [];
      return this.update(cart);
    }
  }
}
