import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  HasOneRepositoryFactory,
  repository,
} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Order, ShoppingCart, User, UserRelations} from '../models';
import {OrderRepository} from './order.repository';
import {ShoppingCartRepository} from './shopping-cart.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly shoppingCart: HasOneRepositoryFactory<
    ShoppingCart,
    typeof User.prototype.id
  >;

  public readonly orders: HasManyRepositoryFactory<
    Order,
    typeof User.prototype.id
  >;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('ShoppingCartRepository')
    protected shoppingCartRepositoryGetter: Getter<ShoppingCartRepository>,
    @repository.getter('OrderRepository')
    protected orderRepositoryGetter: Getter<OrderRepository>,
  ) {
    super(User, dataSource);
    this.orders = this.createHasManyRepositoryFactoryFor(
      'orders',
      orderRepositoryGetter,
    );
    this.registerInclusionResolver('orders', this.orders.inclusionResolver);
    this.shoppingCart = this.createHasOneRepositoryFactoryFor(
      'shoppingCart',
      shoppingCartRepositoryGetter,
    );
    this.registerInclusionResolver(
      'shoppingCart',
      this.shoppingCart.inclusionResolver,
    );
  }
}
