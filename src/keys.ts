import {UserService} from '@loopback/authentication';
import {BindingKey} from '@loopback/context';
import {Credential} from './constants/interfaces';
import {User} from './models';

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<UserService<User, Credential>>(
    'services.user.service',
  );
}
