import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {ShoppingCart, ShoppingCartItem} from '../models';
import {ShoppingCartRepository} from '../repositories';

export class ShoppingCartController {
  constructor(
    @repository(ShoppingCartRepository)
    public shoppingCartRepository: ShoppingCartRepository,
  ) {}

  @post('/shopping-carts', {
    responses: {
      '200': {
        description: 'ShoppingCart model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(ShoppingCart)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCart, {
            title: 'NewShoppingCart',
            exclude: ['userId'],
          }),
        },
      },
    })
    shoppingCart: Omit<ShoppingCart, 'userId'>,
  ): Promise<ShoppingCart> {
    return this.shoppingCartRepository.create(shoppingCart);
  }

  @get('/shopping-carts/count', {
    responses: {
      '200': {
        description: 'ShoppingCart model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ShoppingCart) where?: Where<ShoppingCart>,
  ): Promise<Count> {
    return this.shoppingCartRepository.count(where);
  }

  @get('/shopping-carts', {
    responses: {
      '200': {
        description: 'Array of ShoppingCart model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ShoppingCart, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ShoppingCart) filter?: Filter<ShoppingCart>,
  ): Promise<ShoppingCart[]> {
    return this.shoppingCartRepository.find(filter);
  }

  @patch('/shopping-carts', {
    responses: {
      '200': {
        description: 'ShoppingCart PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCart, {partial: true}),
        },
      },
    })
    shoppingCart: ShoppingCart,
    @param.where(ShoppingCart) where?: Where<ShoppingCart>,
  ): Promise<Count> {
    return this.shoppingCartRepository.updateAll(shoppingCart, where);
  }

  @patch('/shopping-carts/{id}', {
    responses: {
      '204': {
        description: 'ShoppingCart PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCart, {partial: true}),
        },
      },
    })
    shoppingCart: ShoppingCart,
  ): Promise<void> {
    await this.shoppingCartRepository.updateById(id, shoppingCart);
  }

  @put('/shopping-carts/{id}', {
    responses: {
      '204': {
        description: 'ShoppingCart PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() shoppingCart: ShoppingCart,
  ): Promise<void> {
    await this.shoppingCartRepository.replaceById(id, shoppingCart);
  }

  @del('/shopping-carts/{id}', {
    responses: {
      '204': {
        description: 'ShoppingCart DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.shoppingCartRepository.deleteById(id);
  }

  @post('/shopping-carts/{userId}/add')
  async addItem(
    @param.path.string('userId') userId: string,
    @requestBody() cartItem: ShoppingCartItem,
  ): Promise<ShoppingCart> {
    let cart = await this.shoppingCartRepository.findOne({where: {userId}});
    cart = cart ?? new ShoppingCart({userId});
    cart.items = cart.items ?? [];

    const hasThisItem =
      cart.items.findIndex(item => item.productId === cartItem.productId) >= 0;
    if (hasThisItem) {
      cart.items = cart.items.map(item => {
        if (item.productId === cartItem.productId)
          return {...item, quantity: item.quantity + 1} as ShoppingCartItem;
        return item;
      });
    } else {
      cart.items.push(cartItem);
    }

    await this.shoppingCartRepository.updateById(userId, {items: cart.items});

    return cart;
  }

  @get('/shopping-carts/{userId}')
  async getCartByUserId(
    @param.path.string('userId') userId: string,
  ): Promise<ShoppingCart> {
    let cart = await this.shoppingCartRepository.findOne({where: {userId}});
    cart = cart ?? new ShoppingCart({userId});
    return cart;
  }
}
