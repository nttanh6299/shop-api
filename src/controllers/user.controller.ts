import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import _ from 'lodash';
import {Credential, UserToken} from '../constants';
import {User} from '../models';
import {UserRepository} from '../repositories';

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @post('/users', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(User)}},
      },
    },
  })
  async create(
    @requestBody()
    userRequest: Omit<User, 'id'>,
  ): Promise<Pick<User, 'id' | 'name'>> {
    const {email, password} = userRequest;

    if (!email || !password) {
      throw new HttpErrors.Unauthorized('Email and password are required');
    }
    const foundUser = await this.userRepository.findOne({
      where: {email},
    });
    if (foundUser) {
      throw new HttpErrors.Unauthorized('Email is existed');
    }

    const res = await this.userRepository.create(userRequest);

    const createdUser = _.pick(res, ['id', 'name']);
    return createdUser;
  }

  @post('/users/login')
  async login(@requestBody() credential: Credential): Promise<UserToken> {
    const {email, password} = credential;

    if (!email || !password) {
      throw new HttpErrors.Unauthorized('Email and password are required');
    }

    const foundUser = await this.userRepository.findOne({
      where: {email},
    });
    if (!foundUser || (foundUser && password !== foundUser.password)) {
      throw new HttpErrors.Unauthorized('Email or password is invalid');
    }

    const tokenUser: UserToken = {
      token: foundUser.id,
      name: foundUser.name || 'Unnamed',
    };
    return tokenUser;
  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'User model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(User) where?: Where<User>): Promise<Count> {
    return this.userRepository.count(where);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(User) filter?: Filter<User>): Promise<User[]> {
    return this.userRepository.find(filter);
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}')
  async findById(
    @param.path.string('id') id: string,
    @param.filter(User, {exclude: 'where'}) filter?: FilterExcludingWhere<User>,
  ): Promise<UserToken | undefined> {
    const foundUser = await this.userRepository.findById(id, filter);

    if (foundUser) {
      const tokenUser: UserToken = {
        token: foundUser.id,
        name: foundUser.name || 'Unnamed',
      };
      return tokenUser;
    }
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    await this.userRepository.updateById(id, user);
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'User PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.replaceById(id, user);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.userRepository.deleteById(id);
  }
}
