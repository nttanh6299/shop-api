export interface Credential {
  email: string;
  password: string;
}

export interface UserToken {
  token: string;
  name: string;
}
